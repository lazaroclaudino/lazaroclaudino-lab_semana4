using System.Media;

namespace B._4Laboratorio_Semana_4
{
    public partial class Form1 : Form
    {
        Image sprite;
        SoundPlayer somMovimento;
    
        public Form1()
        {
            InitializeComponent();
      
            sprite = Image.FromFile("C:\\Users\\Lázaro\\Documents\\C\\labs\\B.4Laboratorio_Semana_4\\B.4Laboratorio_Semana_4\\bin\\Debug\\sprite.png");
            somMovimento = new SoundPlayer("C:\\Users\\Lázaro\\Documents\\C\\labs\\B.4Laboratorio_Semana_4\\B.4Laboratorio_Semana_4\\bin\\Debug\\whip.wav");      
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawImage(sprite, spriteX, spriteY);
        }

        int spriteX = 100;
        int spriteY = 100;

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Up)
            {
                spriteY -= 10;
                somMovimento.Play();
            }
            else if (keyData == Keys.Down)
            {
                spriteY += 10;
                somMovimento.Play();
            }
      
            Refresh();
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}